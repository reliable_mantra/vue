import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import resource from 'vue-resource'
import './registerServiceWorker'

Vue.config.productionTip = false

Vue.use(resource)
Vue.http.options.root = 'https://vuejs-stock-trader-a7759.firebaseio.com/'

Vue.filter('currency', (value) => {
  return '$' + value.toLocaleString()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
