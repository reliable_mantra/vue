# vue-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Project Root

The dist folder is created after the **npm run build** command and it has to be served over an http server.  
Also, the root has to be set to the dist folder instead of the vue cli root folder that contains the dist folder.

# Vue Instance

```js
var vm = new Vue({
    el: '#app',
    template: '<h1>Hello!</h1>',
    data: {
        param: 10,
        counter: 0
    },
    computed: {
        myParam: function() {
            return this.param > 5 ? 'Greater 5' : 'Smaller than 5';
        }
    },
    watch: {
        counter: function(value) {
            var vm = this;
            setTimeout(function() {
                vm.counter = 0;
            }, 2000);
        }
    },
    methods: {
        exampleMethod() {

        }
    },
    beforeCreate: function() {
        console.log('beforeCreate()');
    },
    created: function() {
        console.log('created()');
    },
    beforeMount: function() {
        console.log('beforeMount()');
    },
    mounted: function() {
        console.log('mounted()');
    },
    beforeUpdate: function() {
        console.log('beforeUpdate()');
    },
    updated: function() {
        console.log('updated()');
    },
    beforeDestroy: function() {
        console.log('beforeDestroy()');
    },
    destroyed: function() {
        console.log('destroyed()');
    },
});

console.log(vm.$data);

vm.method();

vm.$refs.heading.innerText = 'Something else';

vm1.$mount('#app1');
```

# Templating

```vue
<div id="app">
    <h1 v-once>{{ title }}</h1>

    <button @click="buttonClickedMethod"></button>

    <input type="text" v-model="name">
    <p>{{ name }}</p>

    <div :class="[param, {class: true}]"></div>
    <div :style="[param, {backgroundColor: color}]"></div>

    <p v-if="show">You can see me! <span>Hello!</span></p>
    <p v-else>Now you see me!</p>
    <p v-show="show">Do you also see me?</p>

    <ul>
        <li v-for="(ingredient, i) in ingredients" :key="ingredient">{{ ingredient }} ({{ i }})</li>
    </ul>
</div>
```

# Components

```vue
<template>
    <div>
        <p v-if="!server">Please select a Server</p>
        <p v-else>Server #{{ server.id }} selected, Status: {{ server.status }}</p>
        <hr>
        <button @click="resetStatus">Change to Normal</button>

        <ul class="list-group">
            <app-server v-for="server in servers" :server="server"></app-server>
        </ul>

        <h2 slot="title"></h2>
        <p slot="content">Default slot content.</p>
        <slot name="title"></slot>
        <slot name="content"></slot>

        <keep-alive>
            <component :is="selectedComponent">
                Default content.
            </component>
        </keep-alive>
    </div>
</template>
```
```html
<script>
    import { serverBus } from '../../main';
    import Server from './Server.vue';

    export default {
        data: function() {
            return {
                server: null
            }
        },
        props: {
            server: {
                type: Object,
                default: () => ({})
            }
        },
        components: {
            appServer: Server
        },
        methods: {
            serverSelected() {
                serverBus.$emit('serverSelected', this.server)
            },
            resetStatus() {
                this.server.status = 'Normal';
            }
        },
        created() {
            serverBus.$on('serverSelected', (server) => {
                this.server = server;
            });
        },
        destroyed() {
            console.log('Destroyed!');
        },
        deactivated() {
            console.log('Deactivated!');
        },
        activated() {
            console.log('Activated!');
        }
  }
</script>


<style scoped>
</style>
```

### Slots

Slots are used for passing content from outside of a component.

#### Parent Component

```vue
<template>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <app-quote>
                    <h2>The Quote</h2>
                    <p>A wonderful Quote</p>
                </app-quote>
                <app-new-quote>
                    <h2 slot="title">{{ quoteTitle }}</h2>
                    <p slot="content">A wonderful Quote</p>
                </app-new-quote>
            </div>
        </div>
    </div>
</template>
```
```html
<script>
    import Quote from './components/Quote.vue';
    import NewQuote from './components/NewQuote.vue';

    export default {
        data: function() {
            return {
                quoteTitle: 'The Quote'
            };
        },
        components: {
            appQuote: Quote,
            newAppQuote: NewQuote
        }
    }
</script>
```

#### Slot

```vue
<template>
    <div>
        <slot></slot>
    </div>
</template>
```

#### Named Slots

```vue
<template>
    <div>
        <div class="title">
            <slot name="title"></slot>
            <span><slot name="subtitle">The Subtitle</slot></span>
        </div>
        <hr>
        <div class="content">
            <slot name="content"></slot>
        </div>
    </div>
</template>
```

### Dynamic Components

By default the dynamic components are being destroyed and recreated while switching components.

In order for components to not get destroyed you can wrap it in As the component is being kept alive, we use the destroy cycle and if we want to react to navigating away so that another component gets loaded we now have to life cycle hooks:

* deactivated (if we navigated away)
* activated (if a dynamic component is being currently visited) 

#### Parent Component

```vue
<template>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <button @click="selectedComponent = 'appQuote'">Quote</button>
                <button @click="selectedComponent = 'appAuthor'">Author</button>
                <button @click="selectedComponent = 'appNew'">New</button>
                <hr>
                <component :is="selectedComponent"></component>
                <hr>
                <keep-alive>
                    <component :is="selectedComponent"></component>
                </keep-alive>
            </div>
        </div>
    </div>
</template>
```
```html
<script>
    import Quote from './components/Quote.vue';
    import Author from './components/Author.vue';
    import New from './components/New.vue';

    export default {
        data: function() {
            return {
                selectedComponent: 'appQuote'
            };
        },
        components: {
            appQuote: Quote,
            appAuthor: Author,
            appNew: New,
        }
    }
</script>
```

#### Quote

```vue
<template>
    <div>
        <h2>The Quote</h2>
        <p>A wonderful Quote</p>
    </div>
</template>
```
```html
<script>
    export default {
        deactivated() {
            console.log('Deactivated!');
        },
        activated() {
            console.log('Activated!');
        }
    }
</script>
```

#### Author

```vue
<template>
    <div>
        <h3>The Author</h3>
    </div>
</template>
```

#### New Quote

```vue
<template>
    <div>
        <h3>New Quote</h3>
    </div>
</template>
```

# Components Communication

## Unidirectional Flow

#### Using Props for Parent => Child communication

Parent component passing the value

```vue
<template>
    <child-component :name="name"></child-component>
</template>
```
```html
<script>
    import ChildComponent from './ChildComponent.vue';

    export default {
        data: function() {
            return {
                name: 'Max'
            };
        },
        components: {
            childComponent: ChildComponent
        }
    }
</script>
```

Child component receiving the value

```vue
<template>
    <h1>{{ name }}</h1>
</template>
```
```html
<script>
    export default {
        // props: ['name'],
        props: {
            name: {
                type: String,
                // required: true,
                default: 'Max'
            },
            person: {
                type: Object,
                default: function() {
                    return {
                        name: 'Max'
                    }
                }
            }
        },
        methods: {
            switchName() {
                return this.name.split("").reverse().join("");
            }
        }
    }
</script>
```

#### Using Custom Events for Child => Parent communication

Parent Component

```vue
<template>
    <h1>{{ name }}</h1>
    <child-component :name="name" @nameWasReset="name = $event"></child-component>
</template>
```
```html
<script>
    import ChildComponent from './ChildComponent.vue';

    export default {
        data: function() {
            return {
                name: 'Zack'
            };
        },
        components: {
            childComponent: ChildComponent
        }
    }
</script>
```

Child Component emitting an event

```vue
<template>
    <button @click="resetName">Reset Name</button>
</template>
```
```html
<script>
    export default {
        props: ['name'],
        methods: {
            resetName() {
                this.name = 'Max';
                this.$emit('nameWasReset', this.name);
            }
        }
    }
</script>
```

#### Using Callback Functions for Child => Parent communication

Parent Component

```vue
<template>
    <h1>{{ name }}</h1>
    <child-component :name="name" :resetFn="resetName"></child-component>
</template>
```
```html
<script>
    import ChildComponent from './ChildComponent.vue';

    export default {
        data: function() {
            return {
                name: 'Zack'
            };
        },
        components: {
            childComponent: ChildComponent
        },
        methods: {
            resetName(
                this.name = 'Max';
            )
        }
    }
</script>
```

Child Component emitting an event

```vue
<template>
    <button @click="resetFn()">Reset Name</button>
</template>
```
```html
<script>
    export default {
        props: {
            name: {
                type: String
            },
            resetFn: {
                type: Function
            }
        }
    }
</script>
```

## Communication between Sibling Components

#### Using a Custom Event, trough a parent component

Parent Component

```vue
<template>
    <child-edit :userAge="age" @ageWasEdited="age = $event"></child-edit>
    <child-detail :userAge="age"></child-detail>
</template>
```
```html
<script>
    import ChildDetail from './ChildDetail.vue';
    import ChildEdit from './ChildEdit.vue';

    export default {
        data: function() {
            return {
                age: 27
            };
        },
        components: {
            childDetail: ChildDetail,
            childEdit: ChildEdit
        }
    }
</script>
```

Child Edit Component

```vue
<template>
    <button @click="editAge">Edit Age</button>
</template>
```
```html
<script>
    export default {
        props: ['userAge'],
        methods: {
            editAge() {
                this.userAge = 30;
                this.$emit('ageWasEdited', this.userAge);
            };
        }
    }
</script>
```

Child Detail Component

```vue
<template>
    <h1>{{ userAge }}</h1>
</template>
```
```html
<script>
    export default {
        props: ['userAge']
    }
</script>
```

#### Using a Callback Function, trough a parent component (basically the same as the first option)
#### Using an Event Bus for communication 
 
Uses a new Vue instance as a service for communication.

New Vue Instance used as an Event Bus (defined in main.js, before the app vue instance)

```js
export const eventBus = new Vue(
    methods: {
        changeAge(age) {
            this.$emit('ageWasEdited', age);
        }
    }
);

new Vue({
    el: '#app',
    render: h => h(App)
});
```

Parent Component

```vue
<template>
    <child-edit :userAge="age"></child-edit>
    <child-detail :userAge="age"></child-detail>
</template>
```
```html
<script>
    import ChildDetail from './ChildDetail.vue';
    import ChildEdit from './ChildEdit.vue';

    export default {
        data: function() {
            return {
                age: 27
            };
        },
        components: {
            childDetail: ChildDetail,
            childEdit: ChildEdit
        }
    }
</script>
```

Child Edit Component

```vue
<template>
    <button @click="editAge">Edit Age</button>
</template>
```
```html
<script>
    import { eventBus } from '../main';

    export default {
        props: ['userAge'],
        methods: {
            editAge() {
                this.userAge = 30;
                // eventBus.$emit('ageWasEdited', this.userAge);
                eventBus.changeAge(this.userAge);
            }
        }
    }
</script>
```

Child Detail Component

```vue
<template>
    <h1>{{ userAge }}</h1>
</template>
```
```html
<script>
    import { eventBus } from '../main';

    export default {
        props: ['userAge'],
        created() {
            eventBus.$on('ageWasEdited', (age) => {
                this.userAge = age;
            })
        }
    }
</script>
```

# Forms

* :value="param" @input="param = $event.target.value" (Manual v-model)
* v-model="param"
* v-model.lazy, v-model.trim, v-model.number (Modifiers)

```vue
<template>
    <form action="">
        <label for="email">Mail</label>
        <input type="text" id="email" :value="userData.email" @input="userData.email = $event.target.value">

        <label for="password">Password</label>
        <input type="password" id="password" v-model.lazy="userData.password">

        <label for="age">Age</label>
        <input type="number" id="age" v-model="userData.age">

        <!-- Textbox -->
        <label for="message">Message</label>
        <textarea id="message" cols="30" rows="10" v-model="message"></textarea>

        <!-- Checkboxes -->
        <label for="sendmail">
            <input type="checkbox" id="sendmail" v-model="sendMail">
        </label>
        <label for="sendInfomail">
            <input type="checkbox" id="sendInfomail" v-model="sendMail">
        </label>

        <!-- Radio Buttons -->
        <label for="male">
            <input type="radio" id="male" value="Male" v-model="gender"> Male
        </label>
        <label for="female">
            <input type="radio" id="female" value="Female" v-model="gender"> Female
        </label>

        <!-- Dropdown -->
        <label for="priority">Priority</label>
        <select id="priority" v-model="selectedPriority">
            <option v-for="priority in priorities">{{ priority }}</option>
        </select>

        <!-- Custom Component Input -->
        <app-switch v-model="dataSwitch"></app-switch>

        <!-- Submit -->
        <button @click.prevent="submitted">Submit</button>
    </form>

    <h2>Display form values</h2>
    <div>
        <p>Mail: {{ userData.email }}</p>
        <p>Password: {{ userData.password }}</p>
        <p>Age: {{ userData.age }}</p>
        <p style="white-space: pre;">Message: {{ message }}</p>
        <p><strong>Send Mail?</strong></p>
        <ul>
            <li v-for="item in sendMail">{{ item }}</li>
        </ul>
        <p>Gender: {{ gender }}</p>
        <p>Priority: {{ selectedPriority }}</p>
        <p>Switched: {{ dataSwitch }}</p>
    </div>
</template>
```
```html
<script>
    import Switch from './Switch.vue';

    export default {
        data() {
            return {
                userData: {
                    email: '',
                    password: '',
                    age: 27
                },
                message: 'A new Text',
                sendMail: [],
                gender: 'Male',
                selectedPriority: 'High',
                priorities: ['High', 'Medium', 'Low'],
                dataSwitch: true,
                isSubmitted: false
            };
        },
        methods: {
            submitted() {
                this.isSubmitted = true;
            }
        },
        components: {
            appSwitch: Switch
        }
    }
</script>
```

#### Custom Component Input (Switch)

```vue
<template>
    <div>
        <div id="on" @click="switched(true)" :class="{active: value}">On</div>
        <div id="off" @click="switched(false)" :class="{active: !value}">Off</div>
    </div>
</template>
```
```html
<script>
    export default {
        props: ['value'],
        methods: {
            switched(isOn) {
                this.$emit('input', isOn);
            }
        }
    }
</script>
```

# Directives

### Built-in Directives

Directives are generally always identified with the v- at the beginning and then the name of the directive.

* v-bind
* v-if
* v-for
* v-text
* v-html

### Custom Directives

#### Hooks

* bind(el, binding, vnode) - Once Directive is Attached
* inserted(el, binding, vnode) - Inserted in Parend Node
* update(el, binding, vnode, oldVnode) - Once Component is Updated (without Children)
* componentUpdate(el, binding, vnode, oldVnode) - Once Component is Updated (with Children)
* unbind(el, binding, vnode) - Once Directive is Removed

#### Global Directive

Create a directive globally:

```js
// v-highlight
Vue.directive('highlight', {
    bind(el, binding, vnode) {
        // el.style.backgroundColor = 'Green';
        // el.style.backgroundColor = binding.value;
        let delay = 0;
        if (binding.modifiers['delayed']) {
            delay = 3000;
        }
        setTimeout(() => {
            if (binding.arg = 'background') {
                el.style.backgroundColor = binding.value;
            } else {
                el.style.color = binding.value;
            }
        }, delay);
    }
});
```

Use directive inside a component:

```vue
<template>
    <div>
        <p v-highlight:background.delayed="'red'">Some example paragraph.</p>
    </div>
</template>
```

#### Local Directive

```vue
<template>
    <div>
        <p v-local-highlight:background.delayed.blink="'{mainColor: 'red', secondColor: 'green', delay: 500}'">Some example paragraph.</p>
    </div>
</template>
```
```html
<script>
    export default {
        directives: {
            'local-highlight': {
                bind(el, binding, vnode) {
                    // el.style.backgroundColor = 'Green';
                    // el.style.backgroundColor = binding.value;
                    let delay = 0;
                    if (binding.modifiers['delayed']) {
                        delay = 3000;
                    }
                    if (binding.modifiers['blink']) {
                        let mainColor = binding.value.mainColor;
                        let secondColor = binding.value.secondColor;
                        let currentColor = mainColor;
                        setTimeout(() => {
                            setInterval(() => {
                                currentColor = currentColor == secondColor ?  mainColor : secondColor;
                                if (binding.arg = 'background') {
                                    el.style.backgroundColor = currentColor;
                                } else {
                                    el.style.color = currentColor;
                                }
                            }, binding.value.delay);
                        }, delay);
                    } else {
                        setTimeout(() => {
                            if (binding.arg = 'background') {
                                el.style.backgroundColor = binding.value.mainColor;
                            } else {
                                el.style.color = binding.value.mainColor;
                            }
                        }, delay);
                    }
                }
            }
        }
    }
</script>
```

# Filters and Mixins

### Filters

A filter is basically a syntax feature you can use in your template to transform the output in the template. It doesn't transform the data itself, it only transforms what the user sees. A typical example is a filter that takes a string and turns it all to upper case letters. Vue.js doesn't ship with built in filters. 

Often times a computed property is a better solution then using a filter.

Create a filter globally:

```js
// Global filter
Vue.filter('to-lowercase', function (value) {
    return value.toLowerCase();
});
```

Use a globally created filter:

```vue
<template>
    <div>
        <p>{{ text | toUppercase | to-lowercase }}</p>
    </div>
</template>
```
```html
<script>
    // Local filter
    export default {
        data() {
            return {
                text: 'Hello there!'
            }
        },
        filters: {
            toUppercase(value) {
                return value.toUpperCase();
            }
        }
    }
</script>
```

### Mixins

Sometimes we have some code or some data which we want to share among instances or components and for that we got mixins. Vue.js cleverly merges a mixin and the already existing data in an instance where we are adding the mixin. The merging process generally is such that it doesn't destroy data in our vue instance, so the vue instance is always right, but it tries to add new things added by the mixin to the existing instance. It also has, for lifecycle hooks, the behaviour that we are able to provide a lifecycle hook in a mixin and in our component or instance and both will get executed though they have the same name... then the order is mixin 1st then after the component. Also important is that the mixin is not shared, it is not the same place in memory which we then use as a mixin in separate components, instead it is really replicated so each components that includes a particular mixin gets a fresh copy of this object... This means that it is safe for you to access and manipulate data without affecting other places in your application.

#### Local Mixin

Create a mixin:

```js
export const exampleMixin = {
    data() {

    },
    methods: {

    }
}
```

Import a mixin:

```vue
<template>
    <div>

    </div>
</template>
```
```html
<script>
    import { exampleMixin } from './exampleMixin';

    export default {
        mixins: [exampleMixin]
    }
</script>
```

#### Global Mixin

A global mixin is added to every vue instance and thus every component in you application and that is why it is rarely used. Most of the time this is used for creating third party plugins for Vue.js

Create a mixin globally:

```js
Vue.mixin({
    created() {
        console.log('Global Mixin - Created Hook');
    }
});
```

# Routing

#### main.js

```javascript
import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: routes,
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        if (to.hash) {
            return { selector: to.hash };
        }
        return {x: 0, y: 0};
    }
});

router.beforeEach((to, from, next) => {
    console.log('global beforeEach');
    next();
    // next(false);
    // next('/home');
});

router.beforeLeave((to, from, next) => {
    console.log('global beforeLeave');
});

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});
```

#### routes.js

```javascript
import Home from './components/Home.vue';
import Menu from './components/Menu.vue';
// import User from './components/user/User.vue';
// import UserDetail from './components/user/UserDetail.vue';
// import UserEdit from './components/user/UserEdit.vue';

const User = resolve => {
    require.ensure(['./components/user/User.vue'], () => {
        resolve(require('./components/user/User.vue'));
    }, 'user');
};

const UserDetail = resolve => {
    require.ensure(['./components/user/UserDetail.vue'], () => {
        resolve(require('./components/user/UserDetail.vue'));
    }, 'user');
};

const UserEdit = resolve => {
    require.ensure(['./components/user/UserEdit.vue'], () => {
        resolve(require('./components/user/UserEdit.vue'));
    }, 'user');
};

export const routes = [
    { path: '', components: {
        default: Home,
        'header-top': Menu,
        'header-bottom': Menu
    }},
    { path: '/user', component: User, children: [
        { path: ':id', component: UserDetail},
        { path: ':id/edit', component: UserEdit, name: 'userEdit', beforeEnter: (to, from, next) => {
            console.log('inside route setup');
            next();
        }}
    ]},
    { path: '/redirect-me', redirect: '/user' },
    { path: '*', redirect: '/' }
];
```

#### App.vue

```vue
<template>
    <div>
        <h1>Routing</h1>
        <hr>
        <ul>
            <router-link to="/" tag="li" active-class="active" exact><a>Home</a></router-link>
            <router-link to="/user" tag="li" active-class="active"><a>User</a></router-link>
        </ul>
        <hr>
        <button @click="navigateToHome">Go to Home</button>
        <hr>
        <router-view name="header-top"></router-view>
        <router-view></router-view>
        <router-view name="header-bottom"></router-view>
    </div>
</template>
```
```html
<script>
    export default {
        methods: {
            navigateToHome() {
                this.$router.push('/');
            }
        }
    }
</script>
```

#### User.vue

```vue
<template>
    <div>
        <h1>The User Page</h1>
        <hr>
            <ul>
                <router-link tag="li" to="/user/1">User 1</router-link>
                <router-link tag="li" to="/user/2">User 2</router-link>
            </ul>
        <hr>
        <router-view></router-view>
    </div>
</template>
```
```html
<script>
    export default {

    }
</script>
```

#### UserDetail.vue

```vue
<template>
    <div>
        <h1>The User Detail Page</h1>
        <hr>
        <p>Loaded ID: {{ id }}</p>
        <hr>
        <router-link tag="button" :to="`/user/${ $route.params.id }/edit?locale=en&q=100#data`">Edit Link</router-link>
        <router-link tag="button" :to="{link}">Edit Link</router-link>
    </div>
</template>
```
```html
<script>
    export default {
        date() {
            return {
                link: {
                    name: 'userEdit',
                    params: {
                        id: $route.params.id
                    },
                    query: {
                        locale: 'en',
                        q: 100
                    },
                    hash: '#data'
                },
                id: this.$route.params.id
            };
        },
        watch: {
            '$route': (to, from) => {
                this.id = to.params.id;
            }
        }
    }
</script>
```

#### UserEdit.vue

```vue
<template>
    <div>
        <h1>The User Edit Page</h1>
        <hr>
        <p>Locale: {{ locale }}</p>
        <p>Analytics: {{ q }}</p>
        <div style="height: 700px"></div>
        <p id="data">Some extra Data</p>
    </div>
</template>
```
```html
<script>
    export default {
        date() {
            return {
                locale: this.$route.query.locale,
                q: this.$route.query.q
            };
        },
        watch: {
            '$route': (to, from) => {
                this.locale = to.query.locale;
                this.q = to.query.q;
            }
        },
        beforeRouteEnter(to, from, next) {
            // this component vm is not available because the component hasn't been created yet
            console.log('component before route');
            next();
            // you can access this component vm inside next()
            // next(vm => {
            //     vm.link;
            // })
        }
    }
</script>
```

# State Management

Mutations can only be synchronous, that's why you should always use actions with mutations.

#### main.js

```javascript
import Vue from 'vue';
import App from './App.vue';

import { store } from './store/store';

new Vue({
    el: '#app',
    store: store,
    render: h => h(App)
});
```

#### store/store.js

```javascript
import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';

import * as actions from './actions';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        counter: 0,
        value: 0
    },
    getters: {
        doubleCounter: state => {
            return state.counter * 2;
        },
        stringCounter: state => {
            return state.counter + 'clicks';
        },
        value: state => {
            return state.value;
        }
    },
    mutations: {
        increment: (state, payload) => {
            state.counter += payload;
        },
        decrement: (state, payload) => {
            state.counter -= payload;
        },
        updateValue: (state, payload) => {
            state.value = payload;
        }
    },
    actions: {
        increment: (context, payload) => {
            context.commit('increment', payload);
        },
        decrement: ({ commit }, payload) => {
            commit('decrement', payload);
        },
        asyncIncrement: ({commit}, payload) => {
            setTimeout(() => {
                commit('increment', payload);
            }, 1000);
        },
        asyncDecrement: ({commit}, payload) => {
            setTimeout(() => {
                commit('decrement', payload);
            }, 1000);
        },
        updateValue: ({commit}, payload) => {
            commit('updateValue', payload);
        },
        ...actions
    },
    modules: {
        counter
    }
});
```

#### modules/example.js

```javascript
const state = {
    exampleCounter: 0
};

const getters = {
    exampleDoubleCounter: state => {
        return state.counter * 2;
    },
};

const mutations = {
    exampleIncrement: (state, payload) => {
        state.counter += payload;
    },
    exampleDecrement: (state, payload) => {
        state.counter -= payload;
    }
};

const actions = {
    exampleIncrement: (context, payload) => {
        context.commit('exampleIncrement', payload);
    },
    exampleDecrement: ({ commit }, payload) => {
        commit('exampleDecrement', payload);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
```

#### actions.js

```javascript
export const action1 = ({commit}, payload) => {
    commit('updateValue', payload);
};

export const action2 = () => {};
```

#### App.vue

```vue
<template>
    <div>
        <app-result></app-result>
        <app-another-result></app-another-result>
        <hr>
        <app-counter></app-counter>
        <app-another-counter></app-another-counter>
        <hr>
        <input type="text" v-model="value">
        <p>{{ value }}</p>
    </div>
</template>
```
```html
<script>
    import Counter from './components/Counter.vue';
    import AnotherCounter from './components/AnotherCounter.vue';
    import Result from './components/Result.vue';
    import AnotherResult from './components/AnotherResult.vue';

    export default {
        computed: {
            value: {
                get() {
                    return this.$store.getters.value;
                },
                set(value) {
                    this.$store.dispatch('updateValue', value);
                }
            }
        },
        components: {
            appCounter: Counter,
            appAnotherCounter: AnotherCounter,
            appResult: Result,
            appAnotherResult: AnotherResult
        }
    }
</script>
```

#### Counter.vue

```vue
<template>
    <div>
        <button @click="increment">Increment</button>
        <button @click="decrement">Decrement</button>
    </div>
</template>
```
```html
<script>
    export default {
        methods: {
            increment() {
                this.$store.state.conter++;
            },
            decrement() {
                this.$store.state.conter--;
            }
        }
    }
</script>
```

#### AnotherCounter.vue

```vue
<template>
    <div>
        <button @click="increment">Increment</button>
        <button @click="decrement">Decrement</button>
    </div>
</template>
```
```html
<script>
    import { mapActions } from 'vuex';
    // import { mapMutations } from 'vuex';
    export default {
        methods: {
            ...mapActions([
                'increment',
                'decrement'
            ]),
            ownCustomMethod() {

            }
        }

        // methods: {
        //     increment(payload) {
        //         this.$store.dispatch('increment', payload);
        //     },
        //     decrement(payload) {
        //         this.$store.dispatch('decrement', payload);
        //     }
        // }

        // methods: {
        //     ...mapMutations([
        //         'increment',
        //         'decrement'
        //     ]),
        //     ownCustomMethod() {
        //
        //     }
        // }

        // methods: {
        //     increment() {
        //         this.$store.commit('increment');
        //     },
        //     decrement() {
        //         this.$store.commit('decrement');
        //     }
        // }
    }
</script>
```

#### Result.vue

```vue
<template>
    <div>
        <p>Counter is: {{ counter }}</p>
    </div>
</template>
```
```html
<script>
    export default {
        computed: {
            counter() {
                return this.$store.state.counter;
            }
        }
    }
</script>
```

#### AnotherResult.vue

```vue
<template>
    <div>
        <p>Double Counter is: {{ counter }}</p>
        <p>Number of Clicks: {{ clicks }}</p>
    </div>
</template>
```
```html
<script>
    import { mapGetters } from 'vuex';
    export default {
        computed: {
            ...mapGetters({
               counter: 'doubleCounter',
               clicks: 'stringCounter'
           }),
            ourOwnComputedProperty() {

            }
        }

        // computed: mapGetters([
        //     'doubleCounter',
        //     'stringCounter'
        // ])

        // computed: {
        //     counter() {
        //         return this.$store.getters.doubleCounter;
        //     },
        //     clicks() {
        //         return this.$store.getters.stringCounter;
        //     }
        // }
    }
</script>   
```

# Transitions and Animations

* v-if and v-show
* on-load ( attribute)
* switch between elements
* dynamic components

### Transitions 

Transition is a special component which we can use to animate anything between the opening and closing tag. Important here is that you can only animate one element with transition. Another important point is that vue.js will analyze the css set for transition and therefore determine how long the animation runs. 

```vue
<template>
    <div>
        <button @click="show = !show">Show Alert</button>
        <br><br>
        <transition name="fade">
            <div v-if="show">This is some info</div>
        </transition>
    </div>
</template>
```
```html
<script>
    export default {
        data() {
            return {
                show: false
            }
        }
    }
</script>

<style>
    .fade-enter {
        opacity: 0;
    }

    .fade-enter-active {
        transition: opacity 1s;
    }

    .fade-leave {
        /*opacity: 1;*/
    }

    .fade-leave-active {
        transition: opacity 1s;
        opacity: 0;
    }
</style>
```

### Animations

```vue
<template>
    <div>
        <button @click="show = !show">Show Alert</button>
        <br><br>
        <transition name="slide">
            <div v-if="show">This is some info</div>
        </transition>
    </div>
</template>
```
```html
<script>
    export default {
        data() {
            return {
                show: false
            }
        }
    }
</script>

<style>
    .slide-enter {
        /*transform: translateY(20px);*/
    }

    .slide-enter-active {
        animation: slide-in 1s  ease-out forwards;
    }

    .slide-leave {
        /*transform: translate(0);*/
    }

    .slide-leave-active {
        animation: slide-out 1s ease-out forwards;
    }

    @keyframes slide-in {
        from {
            transform: translateY(20px);
        }
        to {
            transform: translateY(0);
        }
    }

    @keyframes slide-in {
        from {
            transform: translateY(0);
        }
        to {
            transform: translateY(20px);
        }
    }
</style>
```

### Mixing Transitions and Animations

Since you have both animation and transition here, vue.js doesn't know which one to use but we can tell vue.js which duration of witch property to use by adding a type property and setting it to either animation or transition since these are the only css properties we can use to animate things. 

```vue
<template>
    <div>
        <button @click="show = !show">Show Alert</button>
        <br><br>
        <transition name="slide" type="animation">
            <div v-if="show">This is some info</div>
        </transition>
    </div>
</template>
```
```html
<script>
    export default {
        data() {
            return {
                show: false
            }
        }
    }
</script>

<style>
    .slide-enter {
        opacity: 0;
        /*transform: translateY(20px);*/
    }

    .slide-enter-active {
        animation: slide-in 1s  ease-out forwards;
        transition: opacity 3s;
    }

    .slide-leave {
        /*transform: translate(0);*/
    }

    .slide-leave-active {
        animation: slide-out 1s ease-out forwards;
        transition: opacity 1s;
        opacity: 0;
    }

    @keyframes slide-in {
        from {
            transform: translateY(20px);
        }
        to {
            transform: translateY(0);
        }
    }

    @keyframes slide-in {
        from {
            transform: translateY(0);
        }
        to {
            transform: translateY(20px);
        }
    }
</style>
```

### Using custom CSS class names

```vue
<template>
    <div>
        <button @click="show = !show">Show Alert</button>
        <br><br>
        <transition
                
                enter-active-class="animated bounce"
                
                leave-active-class="animated shake"
        >
            <div>This is some info</div>
        </transition>
    </div>
</template>
```

### Transitioning between Multiple Elements

For vue.js to be able to differentiate between two elements you need to key them by adding the key property. You will also have to set the mode of switching between the two elements and there are two modes:

* out-in lets the old element animate out first and then animate in the new one
* in-out does the opposite.

```vue
<template>
    <div>
        <transition name="slide" mode="out-in">
            <div v-if="show" key="info">This is some info</div>
            <div v-else key="warning">This is some warning</div>
        </transition>
    </div>
</template>
```

### JS Animations - Listening to Transition Event Hooks

Javascript animations can be mixed with css animations. Transition component emits events at certain point of time and we can listen to these events to execute our own javascript code. 

```vue
<template>
    <div>
        <button @click="load = !load">Load / Remove Element</button>
        <transition
            @before-enter="beforeEnter"
            @enter="enter"
            @after-enter="afterEnter"
            @enter-cancelled="enterCancelled"
            @before-leave="beforeLeave"
            @leave="leave"
            @after-leave="afterLeave"
            @leave-cancelled="leaveCancelled"
            :css="false"
        >
            <div style="width: 300px; height: 100px; background-color: lightgreen" v-if="load"></div>
        </transition>
    </div>
</template>
```
```html
<script>
    export default {
        data() {
            return {
                load: true,
                elementWidth: 100
            };
        },
        methods: {
            beforeEnter(el) {
                console.log('beforeEnter');
                this.elementWidth = 100;
                el.style.width = this.elementWidth + 'px';
            },
            enter(el, done) {
                console.log('enter');
                let round = 1;
                const interval = setInterval(() => {
                    el.style.width = (this.elementWidth + round * 10) + 'px';
                    round++;
                    if (round > 20) {
                        clearInterval(interval);
                        done();
                    }
                }, 20);
            },
            afterEnter(el) {
                console.log('afterEnter');
            },
            enterCancelled(el) {
                console.log('enterCancelled');
            },
            beforeLeave(el) {
                console.log('beforeLeave');
                this.elementWidth = 300;
                el.style.width = this.elementWidth + 'px';
            },
            leave(el, done) {
                console.log('leave');
                let round = 1;
                const interval = setInterval(() => {
                    el.style.width = (this.elementWidth - round * 10) + 'px';
                    round++;
                    if (round > 20) {
                        clearInterval(interval);
                        done();
                    }
                }, 20);
            },
            afterLeave(el) {
                console.log('afterLeave');
            },
            leaveCancelled(el) {
                console.log('leaveCancelled');
            }
        }
    }
</script>
```

### Transitioning group of elements

```vue
<template>
    <div>
        <button @click="addItem">Add Item</button>
        <br><br>
        <transition-group tag="ul" name="slide">
            <li v-for="(number, index) in numbers"
                @click="removeItem(index)"
                :key="number"
            >{{ number }}</li>
        </transition-group>
    </div>
</template>
```
```html
<script>
    export default {
        data() {
            return {
                numbers: [1, 2, 3, 4, 5]
            };
        },
        methods: {
            addItem() {
                const pos = Math.floor(Math.random() * this.numbers.length);
                this.numbers.splice(pos, 0, this.numbers.length + 1);
            },
            removeItem(index) {
                this.numbers.splice(index, 1);
            }
        }
    }
</script>

<style>
    .slide-enter {
        opacity: 0;
        /*transform: translateY(20px);*/
    }

    .slide-enter-active {
        animation: slide-in 1s  ease-out forwards;
        transition: opacity 3s;
    }

    .slide-leave {
        /*transform: translate(0);*/
    }

    .slide-leave-active {
        animation: slide-out 1s ease-out forwards;
        transition: opacity 1s;
        opacity: 0;
        position: absolute;
    }

    .slide-move {
        transition: transform 1s;
    }

    @keyframes slide-in {
        from {
            transform: translateY(20px);
        }
        to {
            transform: translateY(0);
        }
    }

    @keyframes slide-in {
        from {
            transform: translateY(0);
        }
        to {
            transform: translateY(20px);
        }
    }
</style>
```
